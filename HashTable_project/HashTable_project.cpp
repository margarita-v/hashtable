// HashTable_project.cpp : Defines the entry point for the console application.
//

/* ����������� ������ ������ ���� ���-������� (Hash<X,Y>), ��� X - ��� �����, Y - ��� ��������). 
	����� ������ ��������� ������ ���������� � ���-�������, �������� �� ���-�������, ����� � ���-�������,
	���������� ����� ��������� � ���-�������, ������ ���-�������, � ��� �� �������� ������������ �� ���� �������. */

#include "stdafx.h"
#include "iostream"
#include "conio.h"
#include <locale.h>
#include <string>	
using namespace std;

const int MaxSize = 1024;
const int MaxLength = 4096;

// ��������������� ���������, �������� ������� ����� ��������� � �������� ��������, ���������� � ���-�������
struct Plant
{
	int width, height, years;
	Plant(int pWidth = 0, int pHeight = 0, int pYears = 0)
	{
		if (pWidth < 0 || pHeight < 0 || pYears < 0)
			throw bad_exception("�������� ��������� ��������.");
		else
		{
			width = pWidth;
			height = pHeight;
			years = pYears;
		}
	}
	~Plant() { }
	friend ostream &operator<<(ostream& output, const Plant& p)
	{
		output << p.width << ", " << p.height << ", " << p.years << " ";
		return output;
	}
	friend istream &operator>>(istream& input, Plant& p)
	{
		cout << "������� ����� ������ ������, ������ � ������� ��������: -->" << endl;
		input >> p.width >> p.height >> p.years;
		return input;
	}
	bool operator ==(const Plant& p) const
	{
		return p.width == width && p.height == height && p.years == years;
	}
	bool operator != (const Plant& p) const 
	{
		return p.width != width || p.height != height || p.years != years;
	}
};

// ������ ����� ������
template <typename TKey, typename TValue> class Node {
private:
	TKey key;
	TValue value;
	Node<TKey, TValue>* next;

public:
	Node() : key(0), value(0), next(NULL) { }
	Node(TKey pKey, TValue pValue) : key(pKey), value(pValue), next(NULL) { }
	~Node() { }
	TKey get_key()
	{
		return key;
	}
	TValue get_value()
	{
		return value;
	}
	Node<TKey, TValue>* get_next()
	{
		return next;
	}
	////////////////////////////////
	void set_key(const TKey& pKey)
	{
		key = pKey;
	}
	void set_value(const TValue& pValue)
	{
		value = pValue;
	}
	void set_next(Node<TKey, TValue>* pNext)
	{
		next = pNext;
	}
};

// ������ ������
template <typename TKey, typename TValue> class List {
private:
	Node<TKey, TValue>* head;
	Node<TKey, TValue>* curr;
public:
	Node<TKey, TValue>* get_head()
	{
		return head;
	}
	Node<TKey, TValue>* get_curr()
	{
		return curr;
	}
	void set_head(Node<TKey, TValue>* pHead)
	{
		head = pHead;
	}
	void set_curr(Node<TKey, TValue>* pCurr)
	{
		curr = pCurr;
	}
	List() : head(NULL), curr(NULL) { }
	List(TKey pKey, TValue pValue) { head = new Node<TKey, TValue>(pKey, pValue); curr = head; }
	~List()
	{
		//Node<TKey, TValue>* node = head;
		Node<TKey, TValue>* curr = head;
		while (head != NULL)
		{
			delete curr;
			curr = head;
			head = head->get_next();
		}
	}

	// ���������� � ����� ������
	void Insert(Node<TKey, TValue>* pNode)
	{
		Node<TKey, TValue>* prev = NULL;
		Node<TKey, TValue>* curr = head;

		while (curr != NULL)
		{
			prev = curr;
			curr = curr->get_next();
		}

		if (prev == NULL)
			head = pNode;
		else
			prev->set_next(pNode);
		pNode->set_next(curr);
	}
	// �������� �������� �� ������ 
	void Delete(const TKey& pKey)
	{
		Node<TKey, TValue>* prev = NULL;
		Node<TKey, TValue>* curr = head;
		if (curr == NULL)
			return;

		bool find = curr->get_key() == pKey;
		while (curr->get_next() != NULL && !find)
		{
			prev = curr;
			curr = curr->get_next();
			find = curr->get_key() == pKey;
		}
		if (find)
		{
			if (prev == NULL)
				head = head->get_next();
			else
				prev->set_next(curr->get_next());
		}
	}
	// ������ ��������� ������
	void PrintList()
	{
		Node<TKey, TValue>* list = head;
		if (list != NULL)
		{
			for (; list != NULL; list = list->get_next())
				cout << "(" << list->get_key() << "; " << list->get_value() << ") ";
			cout << endl;
		}
	}
	// ������� ������� ����� ������ � ������� ������
	Node<TKey, TValue>* Search(const TKey& pKey)
	{
		Node<TKey, TValue>* list = NULL;
		for (list = head; list != NULL; list = list->get_next())
			if (list->get_key() == pKey)
				return list;
		return NULL;
	}

	int count()
	{
		Node<TKey, TValue>* node = head;
		int c = 0;

		while (node != NULL)
		{
			c++;
			node = node->get_next();
		}
		return c;
	}
	void Next()
	{
		if (curr != NULL)
			curr = curr->get_next();
	}
	void Reset()
	{
		curr = head;
	}
	bool EndOfList()
	{
		return curr == NULL;
	}
};

template <typename TKey, typename TValue> class Hash_Table_Iterator;

template <typename TKey, typename TValue> class Hash_Table {
private:
	unsigned Size;
	List<TKey, TValue>* table;

	// �������, �����������, ���� � ������� ������� � �������� ������
	//bool Find(const TKey& pKey, unsigned &pos);  // pos - ������� ���������� ��������

	// �������, ������������ ������� ���-������� � �������� ������
	Node<TKey, TValue>* Search_node(const TKey& pKey)
	{
		int pos = hash(pKey);
		Node<TKey, TValue>* node = table[pos].Search(pKey);
		return node;
	}
public:
	friend class Hash_Table_Iterator<TKey, TValue>;
	Hash_Table();  
	Hash_Table(unsigned pSize);
	~Hash_Table();  
	//inline int hash(const TKey& pKey) { return pKey & (Size - 1); }
	unsigned hash(const TKey& pKey);
	void Insert(const TKey& pKey, TValue& pValue);
	void Delete(const TKey& pKey);
	void Print() const;
	int Count() const;
	//void Iterator(const TKey& pKey);  // pKey - ���� ��������, � �������� ���������� ������������
	void PrintCount()
	{
		for (unsigned i = 0; i < Size; i++)
			cout << i << " - " << table[i].count() << endl;
	}

	Node<TKey, TValue>* Search(const TKey& pKey)
	{
		return Search_node(pKey);
	}
	
	bool IsEmpty() const
	{
		bool is_empty = true;
		// ���� ��� �������� ������� ��������� � NULL, �� ������� �����
		for (unsigned i = 0; (i < Size) && is_empty; i++)
			is_empty = is_empty && (table[i].get_head() == NULL);
		return is_empty;
	}
};

template <typename TKey, typename TValue> unsigned Hash_Table<TKey, TValue>::hash(const TKey& pKey) 
{
	unsigned char* str = (unsigned char*)((void*)&pKey);
	unsigned int h = 0;
	for (; *str; str++)
	{
		h += (unsigned char)(*str);
		h += (h << 10);
		h ^= (h >> 6);
	}
	h += (h << 3);
	h ^= (h >> 11);
	h += (h << 15);
	return (h % Size);
}

template <typename TKey, typename TValue> Hash_Table<TKey, TValue>::Hash_Table()
{
	Size = MaxSize;
	table = new List<TKey, TValue>[Size];
	/*for (int i = 0; i < Size; i++)
	{
		table[i].set_head(NULL);
	}*/
}

template <typename TKey, typename TValue> Hash_Table<TKey, TValue>::Hash_Table(unsigned pSize)
{
	if (pSize >= 1 && pSize <= MaxSize)
		{
			Size = pSize;
			table = new List<TKey, TValue>[Size];
			/*for (unsigned i = 0; i < Size; i++)
			{
				table[i].set_head(NULL);
			}*/
		}
		else
			throw bad_exception("�������� ������ �������.");
}

template <typename TKey, typename TValue> Hash_Table<TKey, TValue>::~Hash_Table()
{
	Node<TKey, TValue>* node;
	for (unsigned i = 0; i < Size; i++)
		while (table[i].get_head() != NULL)
		{
			node = table[i].get_head();
			table[i].set_head(table[i].get_head()->get_next());
			delete node;
		}
	delete[] table;
}

template <typename TKey, typename TValue> void Hash_Table<TKey, TValue>::Insert(const TKey& pKey, TValue& pValue)
{
	int pos = hash(pKey);
	Node<TKey, TValue>* node = Search_node(pKey);
	if (node != NULL)
	{
		node->set_value(pValue);
		return;
	}
	Node<TKey, TValue>* newElem = new Node<TKey, TValue>(pKey, pValue);
	table[pos].Insert(newElem);
}

template <typename TKey, typename TValue> void Hash_Table<TKey, TValue>::Delete(const TKey& pKey)
{
	int pos = hash(pKey);
	table[pos].Delete(pKey);
}

template <typename TKey, typename TValue> void Hash_Table<TKey, TValue>::Print() const
{
	for (unsigned i = 0; i < Size; i++)
		(table[i]).PrintList();
}

template <typename TKey, typename TValue> int Hash_Table<TKey, TValue>::Count() const
{
	int count = 0;
	Node<TKey, TValue>* list = NULL;
	for (unsigned i = 0; i < Size; i++)
		for (list = (table[i]).get_head(); list != NULL; list = list->get_next())
			count++;
	return count;
}

/*template <typename TKey, typename TValue> Node<TKey, TValue>* Hash_Table<TKey, TValue>::GetNext(const TKey& pKey, int &res)
{
	int pos = hash(pKey);
	Node<TKey, TValue>* node = table[pos].Search(pKey);
	Node<TKey, TValue>* result;
	if (node == NULL) // ������� �� ������
	{
		res = -1;
		return NULL;
	}
	else
	{
		result = table[pos].get_head()->MoveNext();
		//result = table[pos].MoveNext(node);
		if (result == NULL)  // ������ ��������� ������� ������, ������������ �� ��������� ������ �������
		{
			unsigned i = pos + 1;
			while ((i <= Size) && ((i <= Size) != (table[i].get_head() != NULL)))
				i++;
			if (i >= Size)
			{
				res = 1;
				return NULL; // ������ ��������� ������� �������
			}
			else
			{
				result = table[i].get_head();
				return result;
			}
		}
		else // �������� ��������� ������� ������
		{
			return result;
		}
	}
}*/

template <typename TKey, typename TValue> class Hash_Table_Iterator {
private:
	// ��������� �������, ���������� ������
	Hash_Table<TKey, TValue>* HashTable;

	// ������ �������� ���������������� ������
	int pos;

	// ��������� �� ��������������� ������� ������
	List<TKey, TValue>* currentList;

	// ������� ��� ���������� Next
	void SearchNextNode(unsigned pPosition)
	{
		pos = -1;
		if (pPosition > HashTable->Size)
			return;

		for (unsigned i = pPosition; i < HashTable->Size; i++)
			if (HashTable->table[i].get_head() != NULL)
			{
				currentList = &HashTable->table[i];
				currentList->Reset();
				pos = i;
				return;
			}
	}
	bool EndOfList = false;
public:
	Hash_Table_Iterator(Hash_Table<TKey, TValue> &ht) 
	{
		HashTable = &ht;
		SearchNextNode(0); 
	}
	void Next()
	{
		//if (currentList->get_head()->get_next() == NULL)
		//{
			//EndOfList = true;
			//return; // �������� ������ �����������
		//}

		//currentList = currentList->get_head()->get_next();
		currentList->Next();
		if (currentList->EndOfList())
			SearchNextNode(++pos);

		if (pos == -1)
		{
			EndOfList = true;
			return; // �������� ������ �����������
		}
	}
	void Reset()
	{
		currentList->set_curr(HashTable->table[pos].get_head());
	}
	TValue& Value()
	{
		return currentList->get_curr()->get_value();
	}
	TKey& Key()
	{
		return currentList->get_curr()->get_key();
	}
	bool GetEndOfList()
	{
		return EndOfList;
	}
};

bool IsANumber(char *num)
{
	int i;
	int len = strlen(num);
	for (i = 0; i < len && isdigit((unsigned char)num[i]); i++);
	return i == len;
}

void InputDigit(char* message, unsigned &digit, unsigned min, unsigned max)
{
	char num[MaxLength];
	bool correct = false;
	cout << message;
	cin >> num;
	digit = atoi(num);
	correct = (IsANumber(num) && (digit >= min && digit <= max));
	_flushall();
	while (!correct)
	{
		cout << "������! ������� ����� �� " << min << " �� " << max << endl;
		cin >> num;
		digit = atoi(num);
		correct = (IsANumber(num) && (digit >= min && digit <= max));
		_flushall();
	}
}

void Menu()
{
	cout << "\n�������� ��������� ��������." << endl;
	cout << "1 - ���������� �������� � ���-�������" << endl;
	cout << "2 - �������� �������� �� ���-�������" << endl;
	cout << "3 - ����� �������� � ���-�������" << endl;
	cout << "4 - ������ ���-�������" << endl;
	cout << "5 - ������� ���������� ��������� � ���-�������" << endl;
	cout << "6 - ������������ �� ���-�������" << endl;
	cout << "0 - �����\n" << endl;
}

typedef Plant TKey;
typedef Plant TValue;
int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	unsigned size;
	unsigned item;

	InputDigit("������� ������ �������.\n", size, 1, MaxSize);

	Hash_Table<TKey, TValue> Table(size); 
	Node<TKey, TValue>* node = new Node<TKey, TValue>();

	Menu();
	InputDigit("--> ", item, 0, 6);
	while (item)
	{
		switch (item)
		{
		case 1: // ����������
			{
				cout << "������� ���� ��������." << endl;
				TKey k;
				cin >> k;

				cout << "������� �������� ��������." << endl;
				TValue v;
				cin >> v;

				Table.Insert(k, v);
				break;
			}
		case 2: // ��������
			{
				cout << "������� ���� ��������." << endl;
				TKey k;
				cin >> k;
				Table.Delete(k);
				break;
			}
		case 3: // �����
			{
				cout << "������� ���� ��������." << endl;
				TKey k;
				cin >> k;
				//Table->Search(k);
				node = Table.Search(k);
				if (node != NULL)
				{
					cout << "������� ������." << endl;
					cout << "�������� ���������� ��������: " << node->get_value() << endl;
				}
				else
					cout << "������� � ������ ������ �� ������ � �������." << endl;
				break;
			}
		case 4: // ������
			{
				if (!Table.IsEmpty())
					Table.Print(); 
				else
					cout << "������� �����." << endl;
				break;
			}
		case 5: // ������� ����������
			{
				if (!Table.IsEmpty())
					cout << "���������� ��������� ���-������� ����� " << Table.Count() << endl;
				else
					cout << "������� �����." << endl;
				Table.PrintCount();
				break;
			}
		case 6: // ������������
			{
				if (!Table.IsEmpty())
				{
					Hash_Table_Iterator<TKey, TValue> iter(Table);
					for (iter.Reset(); !iter.GetEndOfList(); iter.Next())
					{
						TKey key = iter.Key();
						TValue value = iter.Value();
						cout << "	����:		" << key << endl;
						cout << "	��������:	" << value << endl << endl;
					}
				}
				else
					cout << "������� �����." << endl;
				break;
			}
		}
		Menu();
		InputDigit("--> ", item, 0, 6);
	} 
	cout << "������ ���������." << endl;
	_getch();
	return 0;
}

